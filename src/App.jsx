import React, { useState } from "react";

function App() {
	const [tarea, setTarea] = useState("");
	const [lista, setLista] = useState([]);
	const [contador, setContador] = useState(0);
	const [editar, setEdicion] = useState(false);
	const [id, setId] = useState("");
	const agregarTarea = (e) => {
		e.preventDefault();
		if (!tarea.trim()) {
			console.log("elemento vacio");
			return;
		}
		console.log(tarea);
		setTarea("");
		setLista([...lista, { id: contador, tarea: tarea }]);
	};
	const eliminarTarea = (id) => {
		//hace un nuevo array filtrando el item con el mismo id recibido
		const filtro = lista.filter((item) => item.id !== id);
		setLista(filtro);
	};
	//setea el modo edición y capta la tarea en el formulario
	const editarTarea = (item) => {
		setId(item.id);
		setEdicion(true);
		setTarea(item.tarea);
	};

	//cambia el texto de la tarea
	const edicionTarea = (e) => {
		e.preventDefault();
		if (!tarea.trim()) {
			console.log("elemento vacio");
			return;
		}
		const arrayEditado = lista.map((item) =>
			item.id === id ? { id, tarea: tarea } : item
		);
		setLista(arrayEditado);
		setEdicion(false);
		setTarea("");
		setId("");
	};

	return (
		<div className='container'>
			<h1 className='text-center'> crud simple</h1>
			<div className='row'>
				<div className='col-md-8'>
					<h4 className='text-center'>Lista de Tareas</h4>
					<ul className='list-group'>
						{lista.length === 0 ? (
							<li className='list-group-item text-center'>
								{" "}
								No hay tareas agregadas
							</li>
						) : (
							lista.map((item) => (
								<li className='list-group-item' key={item.id}>
									<span className='lead'>{item.tarea}</span>
									<button
										className='btn btn-danger btn-sm float-right'
										onClick={() => eliminarTarea(item.id)}
									>
										Eliminar
									</button>
									<button
										className='btn btn-warning btn-sm float-right mx-2'
										onClick={() => editarTarea(item)}
									>
										editar
									</button>
								</li>
							))
						)}
					</ul>
				</div>
				<div className='col-md-4'>
					<h4 className='text-center'>
						{editar ? "Editar Tarea" : "Agregar Tarea"}
					</h4>
					<form onSubmit={editar ? edicionTarea : agregarTarea}>
						<input
							id=''
							className='form-control mb-2'
							placeholder='ingrese tarea'
							onChange={(e) => setTarea(e.target.value)}
							value={tarea}
						/>
						{editar ? (
							<button
								className='btn btn-warning btn-block'
								type='submit'
							>
								Editar tarea
							</button>
						) : (
							<button
								className='btn btn-dark btn-block'
								type='submit'
								onClick={() => setContador(contador + 1)}
							>
								Agregar tarea
							</button>
						)}
					</form>
				</div>
			</div>
		</div>
	);
}

export default App;
